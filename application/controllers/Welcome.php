<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function login(){
		//die(print_r($_POST)); - ver o que está sendo enviado pelo metodo post
		$this->load->model('LoginModel','login');
		$this->login->verifica();
		$this->load->view('common/cabecalho');
		$this->load->view('access/login_form');
		$this->load->view('common/rodape');
    /* acessando páginas criadas - localhost/LP2_Sidnei/controlador(ou seja index.php)/welcome(nesse caso)/argumento */
	}
}
